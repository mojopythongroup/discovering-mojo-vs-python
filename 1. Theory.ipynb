{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Theory\n",
    "\n",
    "The goal of this project is to review the languages Python and Mojo, with a focus on comparing them in terms of speed, security, power consumption, and other relevant factors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What is Mojo ??"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mojo is a brand-new programming language designed to tackle tricky problems in making artificial intelligence (AI) work. It's special because it's built from scratch to work really well with all kinds of computer hardware, from regular computers to special AI chips.\n",
    "\n",
    "The idea for Mojo came up when developers were struggling to make their AI platform work smoothly across different types of hardware. They needed a programming language that could handle the complexities of AI systems and work seamlessly with various kinds of computer chips.\n",
    "\n",
    "One of the cool things about Mojo is that it's designed to be super smart about how it compiles code. This means it can make your programs run faster and more efficiently, which is really important for AI stuff.\n",
    "\n",
    "Even though Mojo is all about advanced technology, it's also friendly to people who already know Python, a popular programming language. Mojo is like a big brother to Python, adding extra features and capabilities while still feeling familiar.\n",
    "\n",
    "By being compatible with Python, Mojo makes it easy for developers to use their existing Python code and tools. But at the same time, Mojo brings new solutions to some of the problems that Python has, like making programs run faster and handling complex AI tasks more efficiently.\n",
    "\n",
    "In simple terms, Mojo is like a smart and powerful tool that makes it easier to build cool stuff with artificial intelligence, while still being friendly to people who already know Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Understanding GFLOP\n",
    "\n",
    "In the realm of computer performance, the term **GFLOP** stands for \"Giga-Floating Point Operations Per Second.\" It's a measure of the computational power of a system, particularly in terms of floating-point arithmetic."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success alert--secondary\">\n",
    "\n",
    "Here you can observe a notable surge in the potency of GPUs and their capacity to compute GFLOP/S.\n",
    "\n",
    "It is evident that GPUs surpass CPUs in performing such tasks, and the disparity between 2016 and 2024 is substantial! In 2016, the computing power was approximately 10^4 GFLOPs, whereas now it is closer to 10^5 GFLOPs. Interestingly, the magnitude of this gap remains consistent with the previous gap between 2008 and 2016, which was tenfold.\n",
    "\n",
    "This significant increase is particularly noteworthy because advancing from a weaker product to a more powerful one is relatively easier compared to surpassing an already exceptionally capable product. Therefore, even though the gap persists, it signifies a remarkable achievement in GPU development.\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"img/gflops-sp.png\" alt=\"Mojo libraries\" width=\"600\">\n",
    "<img src=\"img/gflops_benchmark_GPU_crop.png\" alt=\"Mojo libraries\" >"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What is Floating-Point Arithmetic?\n",
    "\n",
    "Floating-point arithmetic is a way to represent and manipulate real numbers in computing. Unlike integers, which can represent whole numbers precisely, floating-point numbers can represent a much wider range of values but with limited precision.\n",
    "\n",
    "### Why Does GFLOP Matter?\n",
    "\n",
    "GFLOP is a measure of how quickly a computer can perform floating-point arithmetic operations. This metric is crucial in fields like scientific computing, where complex mathematical calculations are performed regularly.\n",
    "\n",
    "### Understanding the \"Giga\" Prefix\n",
    "\n",
    "The \"Giga\" prefix in GFLOP indicates that the measurement is in billions (10^9) of floating-point operations per second. This scale gives us an idea of the immense computational power being discussed.\n",
    "\n",
    "### Making Sense of GFLOP\n",
    "\n",
    "Imagine you have a calculator that can perform a billion floating-point operations in one second. That's equivalent to saying it can process one GFLOP. This level of performance is vital for tasks like weather prediction, financial modeling, and simulating physical phenomena.\n",
    "\n",
    "### Comparing Systems Using GFLOP\n",
    "\n",
    "When comparing the computational capabilities of different systems, GFLOP provides a standardized metric. Higher GFLOP ratings generally indicate better computational performance, but other factors such as architecture and efficiency also play significant roles.\n",
    "\n",
    "### Example Calculation\n",
    "\n",
    "Let's consider a hypothetical scenario where we have a supercomputer capable of performing 10 trillion floating-point operations per second. To calculate its GFLOP rating:\n",
    "\n",
    "GFLOP = Total Floating-Point Operations / 10^9\n",
    "\n",
    "GFLOP = 10,000,000,000,000 / 10^9\n",
    "\n",
    "GFLOP = 10,000 GFLOP\n",
    "\n",
    "So, this supercomputer has a rating of 10,000 GFLOP, meaning it can perform 10,000 billion floating-point operations per second.\n",
    "\n",
    "> **_NOTE:_** The mathematical calculations to obtain FLOPs are:\n",
    "\n",
    "> So if my CPU (single-core microprocessor clocked at 2.5 GHz) can achieve 4 FLOPs per clock cycle. My CPU has a theoretical speed of 10 billion FLOPS, denoted as 10 GFLOPS.\n",
    "\n",
    "<img src=\"img/GFLOP_calculation2.svg\" alt=\"Mojo libraries\" > <br >\n",
    "\n",
    "<img src=\"img/GFLOP_calculation.svg\" alt=\"Mojo libraries\" >\n",
    "\n",
    "\n",
    "\n",
    "### Conclusion\n",
    "\n",
    "GFLOP is a crucial measure of computational power, particularly in fields where complex calculations are commonplace. Understanding this concept helps in evaluating and comparing the performance of computing systems effectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Libraries\n",
    "\n",
    "Mojo, being a relatively new language, currently lacks an extensive library ecosystem compared to more established languages like Python. As a result, developers may find that many commonly used libraries are not available in Mojo."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Limited Library Availability\n",
    "\n",
    "There are not many libraries implemented in Mojo. This limitation means that when developers require functionality not available in Mojo, they often have to resort to using Python libraries.\n",
    "\n",
    "For instance, if you need to perform complex mathematical calculations or manipulate data structures beyond the capabilities of Mojo's standard libraries, you may need to leverage Python's extensive library ecosystem.\n",
    "\n",
    "### Lack of Security Libraries\n",
    "\n",
    "One notable absence in Mojo's library ecosystem is the lack of dedicated security libraries. While security is a critical aspect of software development, Mojo currently does not have specialized libraries for tasks such as encryption, authentication, or secure communication protocols.\n",
    "\n",
    "Developers working with Mojo should be aware of this limitation and take appropriate measures to ensure the security of their applications. Depending solely on Python libraries for security-related tasks may not be ideal, as they might not fully integrate with Mojo's runtime environment.\n",
    "\n",
    "### Conclusion\n",
    "\n",
    "The limited availability of libraries in Mojo presents challenges for developers, particularly when it comes to leveraging existing tools and functionality. Additionally, the absence of dedicated security libraries underscores the importance of adopting best practices and exercising caution when handling sensitive data in Mojo applications."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"img/mojo_library.png\" alt=\"Mojo libraries\" width=\"200\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Memory allocation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Differences Between Mojo and Java Memory Allocation\n",
    "\n",
    "**Mojo:**\n",
    "\n",
    "- **Ownership Model:** Mojo utilizes an ownership model where each chunk of memory has only one owner at a time. This ensures deterministic deallocation and prevents errors like use-after-free, double-free, and memory leaks.\n",
    "  \n",
    "- **Automatic Management:** Mojo automatically handles memory allocation and deallocation based on ownership rules, relieving developers from manual memory management burdens.\n",
    "  \n",
    "- **Low Performance Overhead:** Mojo's approach to memory management incurs minimal performance overhead due to its deterministic nature.\n",
    "\n",
    "**Java:**\n",
    "\n",
    "- **Garbage Collection:** Java employs a garbage collector for automatic memory management. The garbage collector periodically deallocates unused heap memory, relieving developers from manual memory management. However, this incurs a performance cost as the garbage collector interrupts program execution.\n",
    "\n",
    "- **Complexity Hiding:** Java hides the complexities of memory management from developers through its garbage collector, reducing the likelihood of errors but potentially impacting performance.\n",
    "\n",
    "- **Potential Performance Trade-offs:** While Java's garbage collector automates memory management, it may not reclaim memory as quickly as manual deallocation in some cases, leading to potential performance trade-offs.\n",
    "\n",
    "### Similarities Between Mojo and Rust Memory Allocation\n",
    "\n",
    "**Mojo:**\n",
    "- **Ownership Model:** Mojo's ownership model ensures deterministic deallocation and prevents common memory-related errors.\n",
    "\n",
    "**Rust:**\n",
    "- **Ownership and Borrowing:** Rust also utilizes an ownership and borrowing model to manage memory. Each value in Rust has a single owner, and borrowing rules ensure safe access to memory.\n",
    "\n",
    "**Similitude Example:**\n",
    "\n",
    "Consider a scenario where multiple parts of a program need access to a shared data structure allocated on the heap. In both Mojo and Rust:\n",
    "\n",
    "```rust\n",
    "fn main() {\n",
    "    let mut data = vec![1, 2, 3]; // data is the owner\n",
    "\n",
    "    // Ownership transfer\n",
    "    let _ = modify_data(&mut data);\n",
    "    // data is still accessible here\n",
    "\n",
    "    // Ownership transfer back\n",
    "    let _ = return_data(data);\n",
    "    // data is reclaimed here\n",
    "}\n",
    "\n",
    "fn modify_data(data: &mut Vec<i32>) -> Vec<i32> {\n",
    "    data.push(4);\n",
    "    data.clone() // ownership transferred\n",
    "}\n",
    "\n",
    "fn return_data(data: Vec<i32>) -> Vec<i32> {\n",
    "    data // ownership transferred back\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we assign s1 to s2, the String data is copied, meaning we copy the pointer, the length, and the capacity that are on the stack. We do not copy the data on the heap that the pointer refers to. \n",
    "Here is an example of the memory management in Rust\n",
    "\n",
    "\n",
    "<img src=\"img/memory_rust.svg\" alt=\"Mojo libraries\" width=\"400\">"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
